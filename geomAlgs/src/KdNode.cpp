//
// Created by radoslav on 1.11.2018.
//

#include <iostream>
#include "KdNode.hpp"
#include "common_math.hpp"
#include <queue>
#include <stack>


auto yaxisSorter = [](const sf::Vector2f& v1, const sf::Vector2f& v2){
    return v1.y < v2.y;
};

auto xaxisSorter = [](const sf::Vector2f& v1, const sf::Vector2f& v2){
    return v1.x < v2.x;
};
/*

KdNode* buildKdTree(std::vector<sf::Vector2f>& points, KdNode* parent, pointsIterator start, pointsIterator end, int depth){
    int axis = depth % 2;
    KdNode* node = new KdNode();
    node->depth = depth;
    node->parent = parent;
    std::sort(start, end, ((axis) ? yaxisSorter : xaxisSorter));
//    std::cout << "working with this range" << std::endl;
//    for (auto p = start; p != end; p++) {
//        std::cout << "el: " << p->x << "\ty: " << p->y << std::endl;
//    }
    long lengthOfSlice = std::distance(start, end);
    pointsIterator medianIterator;

    sf::Vector2f median;
    if (lengthOfSlice > 1) {
        if (lengthOfSlice % 2) {
            medianIterator = start + (lengthOfSlice - 1) / 2;
            median = *medianIterator;
            //median = points[std::distance(points.cbegin(), start) + (lengthOfSlice - 1) / 2];
        } else {
            medianIterator = start + lengthOfSlice / 2;
            median = *medianIterator;
            median.x = (medianIterator->x + (medianIterator - 1)->x) / 2;
            median.y = (medianIterator->y + (medianIterator - 1)->y) / 2;
            //median = points[std::distance(points.cbegin(), stearest nart) + lengthOfSlice / 2 - 1];
        }
    } else {
        median = *start;
    }
    node->data = median;
    if (lengthOfSlice > 1) {
        node->left = buildKdTree(points, node, start, medianIterator, depth + 1);
        node->right = buildKdTree(points, node, medianIterator, end, depth + 1);
    }
    return node;
}

std::pair<sf::Vector2f, sf::Vector2f> getCellBoundariesFor(KdNode* n, float minX, float maxX, float minY, float maxY) {
    bool bMinX, bMinY, bMaxX, bMaxY;
    bMinX = bMinY = bMaxX = bMaxY = false;

    KdNode* curr = n;
    KdNode* parent = n->parent;
    while (parent != nullptr && !(bMinX && bMinY && bMaxX && bMaxY)) {
        if (parent->depth % 2) {
            if (curr == parent->right) {
                if (!bMinY) {
                    minY = parent->data.y;
                    bMinY = true;
                }
            } else {
                if (!bMaxY) {
                    maxY = parent->data.y;
                    bMaxY = true;
                }
            }
        } else {
            if (curr == parent->right) {
                if (!bMinX) {
                    minX = parent->data.x;
                    bMinX = true;
                }
            } else {
                if (!bMaxX) {
                    maxX = parent->data.x;
                    bMaxX = true;
                }
            }
        }
        curr = parent;
        parent = curr->parent;
*/
/*        if (!parent || (bMinX && bMinY && bMaxX && bMaxY)) {
            break;
        }*//*

    }
    return std::make_pair(sf::Vector2f(minX, minY), sf::Vector2f(maxX, maxY));
}

KdNode* findCellMate(KdNode* tree, sf::Vector2f m) {
    int axis = 0; //should be zero for tree root but whatever
    auto node = tree;
    while (node->left || node->right) {
        if (axis) {
            if (m.y < node->data.y) {
                node = node->left;
            } else {
                node = node->right;
            }
            axis = 0;
            continue;
        } else {
            if (m.x < node->data.x) {
                node = node->left;
            } else {
                node = node->right;
            }
            axis = 1;
        }
    }
    return node;
}

bool isLeaf(KdNode* n) {
    return n->left == nullptr && n->right == nullptr;
}

KdNode* nnSearch(KdNode* root, sf::Vector2f p, std::vector<sf::CircleShape>& circles) {
    circles.clear();
    KdNode* cellmate = findCellMate(root, p);
    float currentDistance = distance(p, cellmate->data);
    sf::CircleShape c;
    c.setPosition(sf::Vector2f(p.x - currentDistance, p.y - currentDistance));
    c.setRadius(currentDistance);
    c.setOutlineThickness(2.0f);
    c.setOutlineColor(sf::Color::Yellow);
    c.setFillColor(sf::Color::Transparent);
    circles.push_back(c);
    KdNode* nearest = cellmate;
    KdNode* subtreeRoot = cellmate;
    KdNode* split = nullptr;
    size_t numOfComparisons = 0;

    std::queue<KdNode*> explore;

    while (subtreeRoot != nullptr) {
        while (!explore.empty()) {
            split = explore.front();
            explore.pop();
            if (isLeaf(split)) {
                numOfComparisons++;
               if (distance(p, split->data) < currentDistance) {
                   currentDistance = distance(p, split->data);
                   nearest = split;
                   sf::CircleShape c;
                   c.setPosition(sf::Vector2f(p.x - currentDistance, p.y - currentDistance));
                   c.setPointCount(64);
                   c.setRadius(currentDistance);
                   c.setOutlineThickness(2.0f);
                   c.setOutlineColor(sf::Color::Yellow);
                   c.setFillColor(sf::Color::Transparent);
                   circles.push_back(c);
               }
            } else {
                if (split->depth % 2) {
                    if (split->parent != nullptr) {
                        if (split == split->parent->left) {
                            if (p.x - currentDistance >= split->parent->data.x) {
                                continue;
                            }
                        } else {
                            if (p.x + currentDistance <= split->parent->data.x) {
                                continue;
                            }
                        }
                    }
                    if (p.y < split->data.y) {
                        explore.push(split->left);
                        if (p.y + currentDistance > split->data.y) {
                            explore.push(split->right);
                        }
                    } else {
                        explore.push(split->right);
                        if (p.y - currentDistance < split->data.y) {
                            explore.push(split->left);
                        }
                    }
                } else {
                    if (split->parent != nullptr) {
                        if (split == split->parent->left) {
                            if (p.y - currentDistance >= split->parent->data.y) {
                                continue;
                            }
                        } else {
                            if (p.y + currentDistance <= split->parent->data.y) {
                                continue;
                            }
                        }
                    }
                    if (p.x < split->data.x) {
                        explore.push(split->left);
                        if (p.x + currentDistance > split->data.x) {
                            explore.push(split->right);
                        }
                    } else {
                        explore.push(split->right);
                        if (p.x - currentDistance < split->data.x) {
                            explore.push(split->left);
                        }
                    }
                }
            }
        }
        KdNode* parentOfSubtreeRoot = subtreeRoot->parent;
        if (parentOfSubtreeRoot != nullptr) {
            if (subtreeRoot == parentOfSubtreeRoot->left) {
                explore.push(parentOfSubtreeRoot->right);
            } else {
                explore.push(parentOfSubtreeRoot->left);
            }
        }
        subtreeRoot = parentOfSubtreeRoot;
    }
    std::cout << "Examined " << numOfComparisons << " leaf nodes" << std::endl;
    std::cout << "Circles: " << circles.size() << std::endl;
    return nearest;
}


void collectLeaves(KdNode* root, std::vector<sf::Vector2f>& nodes) {
    std::stack<KdNode*> stack;
    stack.push(root);

    while (!stack.empty()) {
        KdNode* n = stack.top();
        stack.pop();
        if (isLeaf(n)) {
            nodes.push_back(n->data);
        } else {
            if (n->left != nullptr) {
                stack.push(n->left);
            }
            if (n->right != nullptr) {
                stack.push(n->right);
            }
        }
    }
}

std::vector<sf::Vector2f> rangeSearch(KdNode* root, sf::FloatRect range, sf::FloatRect fullRegion) {
    std::vector<sf::Vector2f> results;
    std::stack<KdNode*> nodes;
    std::stack<sf::FloatRect> regions;

    nodes.push(root);
    regions.push(fullRegion);
    size_t numOfComparisons = 0;

    while (!nodes.empty()) {
        KdNode* n = nodes.top();
        nodes.pop();
        sf::FloatRect region = regions.top();
        regions.pop();

        if (isLeaf(n)) {
            numOfComparisons++;
            const auto& p = n->data;
            if (p.x >= range.left && p.x <= range.width && p.y >= range.top && p.y <= range.height) {
                results.push_back(n->data);
            }
        } else {
            if (region.left >= range.left && region.width <= range.width && region.top >= range.top && region.height <= range.height) {
                collectLeaves(n, results);
            } else {
                if (n->depth % 2) {
                    if (n->data.y > range.top) {
                        nodes.push(n->left);
                        sf::FloatRect r(region);
                        r.height = n->data.y;
                        regions.push(r);
                    }
                    if (n->data.y < range.height) {
                        nodes.push(n->right);
                        sf::FloatRect r(region);
                        r.top = n->data.y;
                        regions.push(r);
                    }
                } else {
                    if (n->data.x > range.left) {
                        nodes.push(n->left);
                        sf::FloatRect r(region);
                        r.width = n->data.x;
                        regions.push(r);
                    }
                    if (n->data.x < range.width) {
                        nodes.push(n->right);
                        sf::FloatRect r(region);
                        r.left = n->data.x;
                        regions.push(r);
                    }
                }
            }
        }

    }
    std::cout << "Examined " << numOfComparisons << " data points" << std::endl;
    return results;
}

void computeVisLines(KdNode* root, std::vector<sf::Vector3f>& hlines, std::vector<sf::Vector3f>& vlines, float maxX, float maxY) {
    std::stack<sf::FloatRect> regions;
    std::stack<KdNode*> nodes;

    nodes.push(root);
    regions.push(sf::FloatRect(0, 0, maxX, maxY));

    while (!nodes.empty()) {
        KdNode* n = nodes.top();
        nodes.pop();
        const auto& reg = regions.top();
        regions.pop();
        if (isLeaf(n)) {
            continue;
        }
        if (n->depth % 2) {
            hlines.push_back(sf::Vector3f(n->data.y, reg.left, reg.width));
            sf::FloatRect left(reg);
            sf::FloatRect right(reg);

            left.height = n->data.y;
            right.top = n->data.y;

            nodes.push(n->left);
            nodes.push(n->right);

            regions.push(left);
            regions.push(right);
        } else {
            vlines.push_back(sf::Vector3f(n->data.x, reg.top, reg.height));
            sf::FloatRect left(reg);
            sf::FloatRect right(reg);

            left.width = n->data.x;
            right.left = n->data.x;

            nodes.push(n->left);
            nodes.push(n->right);
            regions.push(left);
            regions.push(right);
        }
    }
}
*/

void KdTree::build(std::vector<sf::Vector2f>& points) {
    this->root = this->buildKdTree(points, std::make_shared<KdNode>(), points.begin(), points.end(), 0);
    this->root->parent.reset();
}

std::shared_ptr<KdNode> KdTree::findCellMate(const sf::Vector2f& p) {
    int axis = 0; //should be zero for tree root but whatever
    auto node = this->root;
    while (node->left || node->right) {
        if (axis) {
            if (p.y < node->data.y) {
                node = node->left;
            } else {
                node = node->right;
            }
            axis = 0;
            continue;
        } else {
            if (p.x < node->data.x) {
                node = node->left;
            } else {
                node = node->right;
            }
            axis = 1;
        }
    }
    return node;
}

std::pair<sf::Vector2f, sf::Vector2f> KdTree::getCellBoundariesFor(std::shared_ptr<KdNode> n, float minX, float maxX, float minY, float maxY) {
    bool bMinX, bMinY, bMaxX, bMaxY;
    bMinX = bMinY = bMaxX = bMaxY = false;

    std::shared_ptr<KdNode> curr = n;
    std::shared_ptr<KdNode> parent = n->parent;
    while (parent && !(bMinX && bMinY && bMaxX && bMaxY)) {
        if (parent->depth % 2) {
            if (curr == parent->right) {
                if (!bMinY) {
                    minY = parent->data.y;
                    bMinY = true;
                }
            } else {
                if (!bMaxY) {
                    maxY = parent->data.y;
                    bMaxY = true;
                }
            }
        } else {
            if (curr == parent->right) {
                if (!bMinX) {
                    minX = parent->data.x;
                    bMinX = true;
                }
            } else {
                if (!bMaxX) {
                    maxX = parent->data.x;
                    bMaxX = true;
                }
            }
        }
        curr = parent;
        parent = curr->parent;
/*        if (!parent || (bMinX && bMinY && bMaxX && bMaxY)) {
            break;
        }*/
    }
    return std::make_pair(sf::Vector2f(minX, minY), sf::Vector2f(maxX, maxY));
}

bool KdTree::isLeaf(std::shared_ptr<KdNode> n) {
    return n->left.get() == nullptr && n->right.get() == nullptr;
}

sf::Vector2f KdTree::nnSearch(sf::Vector2f p, std::vector<sf::CircleShape>& circles) {
    circles.clear();
    std::shared_ptr<KdNode> cellmate = this->findCellMate(p);
    float currentDistance = distance(p, cellmate->data);
    sf::CircleShape c;
    c.setPosition(sf::Vector2f(p.x - currentDistance, p.y - currentDistance));
    c.setRadius(currentDistance);
    c.setOutlineThickness(2.0f);
    c.setOutlineColor(sf::Color::Yellow);
    c.setFillColor(sf::Color::Transparent);
    circles.push_back(c);
    std::shared_ptr<KdNode> nearest = cellmate;
    std::shared_ptr<KdNode> subtreeRoot = cellmate;
    std::shared_ptr<KdNode> split;
    size_t numOfComparisons = 0;

    std::queue<std::shared_ptr<KdNode>> explore;

    while (subtreeRoot) {
        while (!explore.empty()) {
            split = explore.front();
            explore.pop();
            if (isLeaf(split)) {
                numOfComparisons++;
                if (distance(p, split->data) < currentDistance) {
                    currentDistance = distance(p, split->data);
                    nearest = split;
                    sf::CircleShape c;
                    c.setPosition(sf::Vector2f(p.x - currentDistance, p.y - currentDistance));
                    c.setPointCount(64);
                    c.setRadius(currentDistance);
                    c.setOutlineThickness(2.0f);
                    c.setOutlineColor(sf::Color::Yellow);
                    c.setFillColor(sf::Color::Transparent);
                    circles.push_back(c);
                }
            } else {
                if (split->depth % 2) {
                    if (split->parent) {
                        if (split == split->parent->left) {
                            if (p.x - currentDistance >= split->parent->data.x) {
                                continue;
                            }
                        } else {
                            if (p.x + currentDistance <= split->parent->data.x) {
                                continue;
                            }
                        }
                    }
                    if (p.y < split->data.y) {
                        explore.push(split->left);
                        if (p.y + currentDistance > split->data.y) {
                            explore.push(split->right);
                        }
                    } else {
                        explore.push(split->right);
                        if (p.y - currentDistance < split->data.y) {
                            explore.push(split->left);
                        }
                    }
                } else {
                    if (split->parent) {
                        if (split == split->parent->left) {
                            if (p.y - currentDistance >= split->parent->data.y) {
                                continue;
                            }
                        } else {
                            if (p.y + currentDistance <= split->parent->data.y) {
                                continue;
                            }
                        }
                    }
                    if (p.x < split->data.x) {
                        explore.push(split->left);
                        if (p.x + currentDistance > split->data.x) {
                            explore.push(split->right);
                        }
                    } else {
                        explore.push(split->right);
                        if (p.x - currentDistance < split->data.x) {
                            explore.push(split->left);
                        }
                    }
                }
            }
        }
        std::shared_ptr<KdNode> parentOfSubtreeRoot = subtreeRoot->parent;
        if (parentOfSubtreeRoot) {
            if (subtreeRoot == parentOfSubtreeRoot->left) {
                explore.push(parentOfSubtreeRoot->right);
            } else {
                explore.push(parentOfSubtreeRoot->left);
            }
        }
        subtreeRoot = parentOfSubtreeRoot;
    }
    std::cout << "Examined " << numOfComparisons << " leaf nodes" << std::endl;
    std::cout << "Circles: " << circles.size() << std::endl;
    return nearest->data;
}

void KdTree::computeVisLines(float maxX, float maxY) {
    std::stack<sf::FloatRect> regions;
    std::stack<std::shared_ptr<KdNode>> nodes;

    hLines.clear();
    vLines.clear();

    nodes.push(root);
    regions.push(sf::FloatRect(0, 0, maxX, maxY));

    while (!nodes.empty()) {
        std::shared_ptr<KdNode> n = nodes.top();
        nodes.pop();
        const auto& reg = regions.top();
        regions.pop();
        if (isLeaf(n)) {
            continue;
        }
        if (n->depth % 2) {
            hLines.push_back(sf::Vector3f(n->data.y, reg.left, reg.width));
            sf::FloatRect left(reg);
            sf::FloatRect right(reg);

            left.height = n->data.y;
            right.top = n->data.y;

            nodes.push(n->left);
            nodes.push(n->right);

            regions.push(left);
            regions.push(right);
        } else {
            vLines.push_back(sf::Vector3f(n->data.x, reg.top, reg.height));
            sf::FloatRect left(reg);
            sf::FloatRect right(reg);

            left.width = n->data.x;
            right.left = n->data.x;

            nodes.push(n->left);
            nodes.push(n->right);
            regions.push(left);
            regions.push(right);
        }
    }
}

std::vector<sf::Vector2f> KdTree::rangeSearch(sf::FloatRect range, sf::FloatRect fullRegion) {
    std::vector<sf::Vector2f> results;
    std::stack<std::shared_ptr<KdNode>> nodes;
    std::stack<sf::FloatRect> regions;

    nodes.push(root);
    regions.push(fullRegion);
    size_t numOfComparisons = 0;

    while (!nodes.empty()) {
        std::shared_ptr<KdNode> n = nodes.top();
        nodes.pop();
        sf::FloatRect region = regions.top();
        regions.pop();

        if (isLeaf(n)) {
            numOfComparisons++;
            const auto& p = n->data;
            if (p.x >= range.left && p.x <= range.width && p.y >= range.top && p.y <= range.height) {
                results.push_back(n->data);
            }
        } else {
            if (region.left >= range.left && region.width <= range.width && region.top >= range.top && region.height <= range.height) {
                this->collectLeaves(n, results);
            } else {
                if (n->depth % 2) {
                    if (n->data.y > range.top) {
                        nodes.push(n->left);
                        sf::FloatRect r(region);
                        r.height = n->data.y;
                        regions.push(r);
                    }
                    if (n->data.y < range.height) {
                        nodes.push(n->right);
                        sf::FloatRect r(region);
                        r.top = n->data.y;
                        regions.push(r);
                    }
                } else {
                    if (n->data.x > range.left) {
                        nodes.push(n->left);
                        sf::FloatRect r(region);
                        r.width = n->data.x;
                        regions.push(r);
                    }
                    if (n->data.x < range.width) {
                        nodes.push(n->right);
                        sf::FloatRect r(region);
                        r.left = n->data.x;
                        regions.push(r);
                    }
                }
            }
        }

    }
    std::cout << "Examined " << numOfComparisons << " data points" << std::endl;
    return results;
}

std::shared_ptr<KdNode> KdTree::buildKdTree(std::vector<sf::Vector2f>& points, std::shared_ptr<KdNode> parent, pointsIterator start, pointsIterator end, int depth){
    int axis = depth % 2;
    std::shared_ptr<KdNode> node = std::make_shared<KdNode>();
    node->depth = depth;
    node->parent = parent;
    std::sort(start, end, ((axis) ? yaxisSorter : xaxisSorter));
//    std::cout << "working with this range" << std::endl;
//    for (auto p = start; p != end; p++) {
//        std::cout << "el: " << p->x << "\ty: " << p->y << std::endl;
//    }
    long lengthOfSlice = std::distance(start, end);
    pointsIterator medianIterator;

    sf::Vector2f median;
    if (lengthOfSlice > 1) {
        if (lengthOfSlice % 2) {
            medianIterator = start + (lengthOfSlice - 1) / 2;
            median = *medianIterator;
            //median = points[std::distance(points.cbegin(), start) + (lengthOfSlice - 1) / 2];
        } else {
            medianIterator = start + lengthOfSlice / 2;
            median = *medianIterator;
            median.x = (medianIterator->x + (medianIterator - 1)->x) / 2;
            median.y = (medianIterator->y + (medianIterator - 1)->y) / 2;
            //median = points[std::distance(points.cbegin(), stearest nart) + lengthOfSlice / 2 - 1];
        }
    } else {
        median = *start;
    }
    node->data = median;
    if (lengthOfSlice > 1) {
        node->left = buildKdTree(points, node, start, medianIterator, depth + 1);
        node->right = buildKdTree(points, node, medianIterator, end, depth + 1);
    }
    return node;
}

void KdTree::collectLeaves(std::shared_ptr<KdNode> root, std::vector<sf::Vector2f>& nodes) {
    std::stack<std::shared_ptr<KdNode>> stack;
    stack.push(root);

    while (!stack.empty()) {
        std::shared_ptr<KdNode> n = stack.top();
        stack.pop();
        if (isLeaf(n)) {
            nodes.push_back(n->data);
        } else {
            if (n->left) {
                stack.push(n->left);
            }
            if (n->right) {
                stack.push(n->right);
            }
        }
    }
}
