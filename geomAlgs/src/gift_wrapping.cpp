#include "gift_wrapping.hpp"
#include "common_math.hpp"

void gift_wrapping(const std::vector<sf::Vector2f>& points, std::vector<sf::Vector2f>& convex_hull){ 
    if (points.size() < 2) {
        return; 
    }
    convex_hull.clear();
    if (points.size() == 0){
        return;
    }
    auto max_x = std::max_element(points.begin(), points.end(),
    [](sf::Vector2f p1, sf::Vector2f p2) {
        return p1.x < p2.x;
    });

    sf::Vector2f pivot_vec(0.f, -1.f);
    auto pivot = &(*max_x);
    do {
        float min_angle = 2 * M_PI;
        sf::Vector2f min_angle_vec;
        const sf::Vector2f* next_pivot = nullptr;
        for (auto it = points.begin(); it != points.end(); ++it){
            if (&(*it) == pivot){
                continue;
            }
            sf::Vector2f v(it->x - pivot->x, it->y - pivot->y);
            float curr_angle = angle_between(pivot_vec, v);
            if (curr_angle - min_angle < 0.0f){
                min_angle = curr_angle;
                min_angle_vec = v;
                next_pivot = &(*it);
            }
        }
        convex_hull.push_back(*next_pivot);
        pivot_vec = min_angle_vec;
        pivot = next_pivot;
        min_angle = 2 * M_PI;
    } while (pivot != &(*max_x));
    //std::cout << "Convex hull has " << convex_hull.size() << " elements" << std::endl;
}