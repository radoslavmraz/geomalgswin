//
// Created by radoslav on 28.12.2018.
//

#include <algorithm>
#include "triangulation.hpp"
#include "common_math.hpp"
#include <stack>

auto comparator = [](const sf::Vector2f& v1, const sf::Vector2f& v2) {
	return (v1.y < v2.y || (abs(v1.y - v2.y) < 0.001 && v1.x < v2.x));
};


bool areOnTheSameChain(const sf::Vector2f& vi, const sf::Vector2f& vtop, const std::vector<sf::Vector2f>& points, std::vector<sf::Vector2f>::iterator min, std::vector<sf::Vector2f>::iterator max, bool isRightFirst) {
	auto vTopIt = std::find_if(points.cbegin(), points.cend(), [&vtop](const sf::Vector2f& v_) { return distance(vtop, v_) < 0.001; });

	if (vTopIt == min) {
		return true;
	}

	auto v1It = std::find_if(points.cbegin(), points.cend(), [&vi](const sf::Vector2f& v_) { return distance(vi, v_) < 0.001; });

	return (vTopIt <= max && v1It <= max) || (vTopIt >= max && v1It >= max);
}

void triangulatePolygon(std::vector<sf::Vector2f>& points, std::vector<Line>& edges) {
	if (points.size() < 3) {
		return;
	}
	std::vector<sf::Vector2f> sorted;

	edges.clear();

	std::copy(points.begin(), points.end(), std::back_inserter(sorted));

	std::sort(sorted.begin(), sorted.end(), comparator);

	auto firstVertex = std::min_element(points.begin(), points.end(), comparator); // this is the topmost vertex, minimal in the lexicographical 

	auto second = (firstVertex == points.cend() - 1) ? points[0] : *(firstVertex + 1); // this is the vertex that was created after firstVertex, in chronological order, not lexicographical order
	auto last = (firstVertex == points.cbegin()) ? *(points.cend() - 1) : *(firstVertex - 1); // this is the vertex that was created as the last, connected to firstVertex

	// now we determine whether the second vertex belongs to left or right chain. Then all vertices(from the vector `points`) between firstVertex and the maximum vertex in lexic. order will be added to left(right) chain and the rest to right(left) chain.
	// `second` is from left chain if the vectors v1=(second - firstVertex), v2=(last - firstVertex) and v1 x v2 make lefthanded coordinate system

	// first let's left rotate the `points` vector so that firstVertex is in the 0th place
	std::rotate(points.begin(), firstVertex, points.end());

	auto minMax = std::minmax_element(points.begin(), points.end(), comparator); // minimal and maximal elements in `points` vector w.r.t lexicographical ordering, minMax.first is minimal, minMax.second is maximal
	// Now all vertices from 0 to position_of(minMax.second) are from one chain and all vertices from position_of(minMax.second) to last_element of `points` are from the other chain

	bool isRightFirst = is_right_turn(*minMax.first, second, last); // do vertices from `points` in the range 0..position_of(minMax.second) correspond to right chain or otherwise?

	std::stack<sf::Vector2f> stack;

	int i = 0;
	stack.push(sorted[i++]);
	stack.push(sorted[i]); //insert v1, v2
	edges.emplace_back(sorted[0], sorted[1]);
	for (i = 2; i < sorted.size(); ++i) {
		auto vi = sorted[i];
		auto top = stack.top(); //no popping has been done, yet

		bool vi_right = (isRightFirst == (std::find_if(points.begin(), points.end(), [&vi](const sf::Vector2f& _v) { return distance(vi, _v) < 0.001; }) <= minMax.second));
		if (areOnTheSameChain(vi, top, points, minMax.first, minMax.second, isRightFirst)) {//if the vi is on the chain as all other vertices in the stack
			Line last_edge = std::make_pair(vi, top);
			edges.push_back(last_edge);
			stack.pop();
			auto vj = stack.top();
			sf::Vector2f vk = top;
			while (!stack.empty() && ((vi_right && !is_right_turn(vi, last_edge.second, vj)) || (!vi_right && is_right_turn(vi, last_edge.second, vj)))) {
				edges.push_back(std::make_pair(vi, vj));
				last_edge = edges[edges.size() - 1];
				vk = vj;
				stack.pop();
				if (!stack.empty())
					vj = stack.top();
			}
			stack.push(vk);
			stack.push(vi);
		}
		else {
			while (stack.size() > 1) {
				auto vj = stack.top();
				stack.pop();
				edges.push_back(std::make_pair(vi, vj));
			}
			stack.pop();
			stack.push(top);
			stack.push(vi);
		}
	}
}

bool is_from_right_chain(const sf::Vector2f& v, const std::vector<sf::Vector2f>& right){
    return std::find_if(right.cbegin(), right.cend(), [&v](const sf::Vector2f& v_){
        return distance(v, v_) < 0.001;
    }) != right.cend();
}
