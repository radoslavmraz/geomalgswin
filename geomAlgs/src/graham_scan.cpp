#include "graham_scan.hpp"
#include <deque>
#include "common_math.hpp"
#include <algorithm>

void normalize(sf::Vector2f& v){
    float mag = sqrt_magnitude(v);
    v.x /= mag;
    v.y /= mag;
}

bool is_right_turn(std::deque<sf::Vector2f>& s){
    auto last = s.back();
    s.pop_back();
    auto middle = s.back();
    s.pop_back();
    auto first = s.back();
    bool is_right = is_right_turn(first, middle, last);
    s.push_back(middle);
    s.push_back(last);
    return is_right;
}

void graham_scan(std::vector<sf::Vector2f>& points, std::vector<sf::Vector2f>& convex_hull){
    // first let's sort according to x-coords
    if (points.size() < 3){
        return;
    }
    convex_hull.clear();
    std::sort(points.begin(), points.end(),
        [] (const sf::Vector2f& v1, const sf::Vector2f& v2){
            return v1.x < v2.x;
        }   
    );
    // now let's sort wrt y-coords
   /* std::sort(points.begin(), points.end(),
        [] (const sf::Vector2f& v1, const sf::Vector2f& v2){
            return v1.y < v2.y;
        }
    );*/

    std::deque<sf::Vector2f> upper_hull;
    upper_hull.push_back(points[0]);
    upper_hull.push_back(points[1]);

    for (int i = 2; i < points.size(); ++i){
        upper_hull.push_back(points[i]);
        while (upper_hull.size() > 2 && !is_right_turn(upper_hull)){
            auto last = upper_hull.back();
            upper_hull.pop_back();
            upper_hull.pop_back();
            upper_hull.push_back(last);
        }
    }
    std::deque<sf::Vector2f> lower_hull;
    lower_hull.push_back(upper_hull.back());
    auto last = upper_hull.back();
    upper_hull.pop_back();
    lower_hull.push_back(upper_hull.back());
    upper_hull.push_back(last);

    for (int i = points.size() - 2; i >= 0; --i){
        lower_hull.push_back(points[i]);
        while (lower_hull.size() > 2 && !is_right_turn(lower_hull)){
            auto last = lower_hull.back();
            lower_hull.pop_back();
            lower_hull.pop_back();
            lower_hull.push_back(last);
        }
    }
    lower_hull.pop_back();
    lower_hull.pop_front();
    convex_hull.insert(convex_hull.begin(), upper_hull.begin(), upper_hull.end());
    convex_hull.insert(convex_hull.end(), lower_hull.begin(), lower_hull.end());
}