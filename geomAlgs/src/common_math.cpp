#include "common_math.hpp"
#include <cmath>

float angle_between(const sf::Vector2f& v1, const sf::Vector2f& v2){
    return std::acos(cos_of_angle(v1, v2));
}

float cos_of_angle(const sf::Vector2f& v1, const sf::Vector2f& v2){
    return dot_product(v1, v2) / (sqrt_magnitude(v1) * sqrt_magnitude(v2));
}

float dot_product(const sf::Vector2f& v1, const sf::Vector2f& v2){
    return v1.x * v2.x + v1.y * v2.y;
}

float sqrt_magnitude(const sf::Vector2f& v){
    return std::sqrt(v.x * v.x + v.y * v.y);
}

bool is_right_turn(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3){
    sf::Vector2f v1(p2.x - p1.x, p2.y - p1.y);
    sf::Vector2f v2(p3.x - p1.x, p3.y - p1.y);
    normalize(v1);
    normalize(v2);
    // let's check what-hand coordinate system vectors v1, v2 form
    // we want to compute the cross product of these vectors, but we are only interested in the 'z' components which is easily computed below
    // and checked if greater than zero -> that indicates that this is in fact right-hand turn
    return v1.x * v2.y - v1.y * v2.x > 0.0f;

}

float distance(sf::Vector2f p1, sf::Vector2f p2) {
    return sqrt_magnitude(p1 - p2);
}

float pseudoCrossProduct(sf::Vector2f p1, sf::Vector2f p2, sf::Vector2f p3) {
    sf::Vector2f v1 = p2 - p1;
    sf::Vector2f v2 = p3 - p1;

    normalize(v1);
    normalize(v2);
    return v1.x * v2.y - v1.y * v2.x;
}

bool equal(const sf::Vector2f& p1, const sf::Vector2f& p2) {
    return distance(p1, p2) < 0.001;
}


sf::Vector2f perpendicular(const sf::Vector2f& v) {
	sf::Vector2f w;
	w.x = v.y;
	w.y = -v.x;
	return w;
}