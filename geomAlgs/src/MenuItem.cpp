//
// Created by radoslav on 28.12.2018.
//

#include "MenuItem.hpp"

MenuItem::MenuItem(const std::string& str, sf::Font& f, clickHandler clck)
: text(str, f, 22), handler(clck)
{
    initRect();
}

MenuItem::MenuItem(const std::string& str, sf::Font& f, bool* boolSwtch)
: text(str, f, 22), boolSwitch(boolSwtch)
{
    initRect();
}

void MenuItem::initRect() {
    const auto& textRect = text.getGlobalBounds();
    this->rect.width = textRect.width + 80;
    this->rect.height = textRect.height + textRect.height / 2;
    background.setSize(sf::Vector2f(rect.width, rect.height));
    background.setFillColor(sf::Color::White);
    if (boolSwitch != nullptr) {
        background.setFillColor((*boolSwitch) ? sf::Color::Green : sf::Color::White);
    }
    background.setOutlineColor(sf::Color::Black);
    background.setOutlineThickness(1.0f);
    text.setFillColor(sf::Color::Black);
}

bool MenuItem::checkAndHandleClick(const sf::Vector2f& p) {
    if (rect.contains(p)) {
        if (boolSwitch != nullptr) {
            *boolSwitch = !(*boolSwitch);
            background.setFillColor((*boolSwitch) ? sf::Color::Green : sf::Color::White);
        } else if (handler != nullptr){
            handler();
        }
        return true;
    }
    return false;
}

void MenuItem::setPosition(const sf::Vector2f& pos) {
    this->rect.left = pos.x;
    this->rect.top = pos.y;
    this->background.setPosition(pos.x, pos.y);
    this->background.setSize(sf::Vector2f(rect.width, rect.height));
    this->text.setPosition(this->rect.left + this->rect.width / 2 - this->text.getGlobalBounds().width / 2,
            this->rect.top - 0.65 * this->rect.height + this->text.getGlobalBounds().height / 2);
}

void MenuItem::draw(sf::RenderWindow& w)const {
    w.draw(background);
    w.draw(text);
}

void MenuItem::pushSubItem(MenuItem item) {
    subItems.push_back(item);
}

void MenuItem::setMenuItemRect(sf::FloatRect r) {
    this->rect = r;
    this->background.setSize(sf::Vector2f(r.width, r.height));
}