//
// Created by radoslav on 29.11.2018.
//
#include "delaunay.hpp"
#include "common_math.hpp"
#include "triangulation.hpp"
#include <cmath>
#include <random>
#include <chrono>
#include <algorithm>
#include <thread>
#include <iostream>
#include <map>
#include <unordered_map>
#include <queue>


bool Circle::isInside(const sf::Vector2f& p)const {
    return std::pow(distance(p, c), 2) - r * r < 0.0;
}

Circle circumCircle(sf::Vector2f p1, sf::Vector2f p2, sf::Vector2f p3) {
    float cp = pseudoCrossProduct(p1, p2, p3);
    Circle c;
    c.r = -42;
    if (fabsf(cp) > 0.00001) {
        auto p1Sq = dot_product(p1, p1);
        auto p2Sq = dot_product(p2, p2);
        auto p3Sq = dot_product(p3, p3);

        auto num = p1Sq * (p2.y - p3.y) + p2Sq * (p3.y - p1.y) + p3Sq * (p1.y - p2.y);
        auto cx = num / (2 * cp);
        num = p1Sq * (p3.x - p2.x) + p2Sq * (p1.x - p3.x) + p3Sq * (p2.x - p1.x);
        auto cy = num / (2 * cp);
        c.c = sf::Vector2f(cx, cy);
        c.r = distance(c.c, p1);

        auto _p2 = p2 - p1;
        auto _p3 = p3 - p1;
        auto d = 2 * (_p2.x * _p3.y - _p2.y * _p3.x);
        c.c.x = (_p3.y * (_p2.x * _p2.x + _p2.y * _p2.y) - _p2.y * (_p3.x * _p3.x + _p3.y * _p3.y)) / d;
        c.c.y = (_p2.x * (_p3.x * _p3.x + _p3.y * _p3.y) - _p3.x * (_p2.x * _p2.x + _p2.y * _p2.y)) / d;
        c.r = std::sqrt(std::pow(c.c.x, 2) + std::pow(c.c.y, 2));
        c.c += p1;
    }
    return c;
}

size_t chooseRandomPointIdx(std::vector<sf::Vector2f>& points) {
    std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<short> point_distro(0, points.size());

    return point_distro(generator);
}

VertexPtr getClosestPoint(std::vector<VertexPtr>& points, sf::Vector2f p) {
    VertexPtr b{nullptr};
    float min = 9999999999999999;
    for (auto& v : points) {
        if (equal(v->p, p)) {
            continue;
        }
        float dist = distance(v->p, p);
        if (dist < min) {
            min = dist;
            b = v;
        }
    }
    return b;
}


float delaunayDistance(const EdgePtr e, sf::Vector2f& p, Circle c) {
    sf::Vector2f v1 = e->b - e->a;
    sf::Vector2f v2 = p - e->a;

    float cp1 = pseudoCrossProduct(e->a, e->b, p);
    float cp2 = pseudoCrossProduct(e->a, e->b, c.c);

    return (cp1 * cp2 < 0.0) ? -c.r : c.r;
}

Circle circumC;
bool smallestDelDistPoint(std::vector<VertexPtr>& points, const EdgePtr e, VertexPtr& closest) {
    float mindelDist = 42000000;
    bool found = false;

    for (auto& p : points) {
        if (pseudoCrossProduct(e->a, e->b, p->p) > 0.0 || distance(p->p, e->a) < 0.001 || distance(p->p, e->b) < 0.001) {
            continue;
        }
        Circle c = circumCircle(e->a, e->b, p->p);
        float delDist = delaunayDistance(e, p->p, c);
        if (delDist < mindelDist) {
            closest = p;
            mindelDist = delDist;
            circumC = c;
            found = true;
        }
    }
    return found;
}



void delaunay_triangulation2(std::vector<sf::Vector2f>& points, std::vector<EdgePtr>& ael, std::vector<EdgePtr>& dl, std::vector<FacePtr>& faces, std::vector<VertexPtr>& verts) {
	if (points.size() < 3) {
		return;
	}
    generateVertices(points, verts);
    //first fetch a random vertex from points
    auto p1 = verts[0];
    //now get the closest vertex to p1
    auto p2 = getClosestPoint(verts, p1->p);
    //construct new edge p1p2
    EdgePtr e = std::make_shared<Edge>();
    e->a = p1->p;
    e->b = p2->p;
    e->v1 = p1;
    e->v2 = p2;
    VertexPtr p;
    //now get point with smallest delaunay distance to the edge e that is left of it
    if (!smallestDelDistPoint(verts, e, p)) { //well if we didn't find such a point then switch orientation of the edge and look again
        e->v1 = p2;
        e->v2 = p1;
        e->a = p2->p;
        e->b = p1->p;
        smallestDelDistPoint(verts, e, p);
    }
    //now we should have 3 points which constitute a face, let's construct it and for each its edge let's construct opposite edge (for edges that make up convex hull we will have unnecessary edges, yes, well that's real life
    facify(e, p, ael, faces, dl);
    //to each edge construct an opposite edge
    opposify(e, ael);
    opposify(e->next.lock(), ael);
    opposify(e->prev.lock(), ael);

    while (!ael.empty()) {
        e = ael[0];
        if (smallestDelDistPoint(verts, e, p)) {
            facify(e, p, ael, faces, dl);
            if (e->next.lock()->twin.lock().get() == nullptr) {
                opposify(e->next.lock(), ael);
//                ael.push_back(e->next.lock()->opposite.lock());
            }
            if (e->prev.lock()->twin.lock().get() == nullptr) {
                opposify(e->prev.lock(), ael);
//                ael.push_back(e->prev.lock()->opposite.lock());
            }
		}
		else {
			dl.push_back(e); //pushing edge from convex hull
		}
        ael.erase(ael.begin());
    }
}


void facify(EdgePtr e, VertexPtr v, std::vector<EdgePtr>& ael, std::vector<FacePtr>& faces, std::vector<EdgePtr>& dl) {

    auto enextIt = std::find_if(ael.begin(), ael.end(), [=] (const EdgePtr& f) {
       return equal(f->a, e->b) && equal(f->b, v->p);
    });

    EdgePtr enext{nullptr};
    EdgePtr eprev{nullptr};
    if (enextIt != ael.end()) {
        enext = *enextIt;
        ael.erase(enextIt);
    } else {
        enext = std::make_shared<Edge>();
        enext->a = e->b;
        enext->b = v->p; //p;
        enext->v1 = e->v2;
        enext->v2 = v;
    }

    auto eprevIt = std::find_if(ael.begin(), ael.end(), [=] (const EdgePtr& f) {
        return equal(f->a, v->p) && equal(f->b, e->a);
    });
    if (eprevIt != ael.end()) {
        eprev = *eprevIt;
        ael.erase(eprevIt);
    } else {
        eprev = std::make_shared<Edge>();
        eprev->a = v->p;//p;
        eprev->b = e->a;
        eprev->v1 = v;
        eprev->v2 = e->v1;
    }

    e->next = enext;
    e->prev = eprev;
    enext->next = eprev;
    eprev->prev = enext;
    eprev->next = e;
    enext->prev = e;
    FacePtr f = std::make_shared<Face>();
    f->c = circumC;
    faces.push_back(f);
    e->face = f;
    enext->face = eprev->face = e->face;
    auto face = e->face.lock();
	auto& vec = face->edges;
	vec.push_back(e);
	vec.push_back(enext);
	vec.push_back(eprev);
	if (vec.capacity() > 3) {
		vec.shrink_to_fit();
	}

    e->v1.lock()->leavingEdge = e;
    e->v2.lock()->leavingEdge = enext;
    v->leavingEdge = eprev;
    dl.push_back(e); 
	dl.push_back(enext); dl.push_back(eprev);
}

void opposify(EdgePtr e, std::vector<EdgePtr>& ael) {
    EdgePtr op = std::make_shared<Edge>();
    ael.push_back(op);
    e->twin = op;
    op->twin = e;
    op->a = e->b;
    op->b = e->a;
    op->v1 = e->v2;
    op->v2 = e->v1;
}

void generateVertices(const std::vector<sf::Vector2f>& points, std::vector<VertexPtr>& verts) {
    std::for_each(points.cbegin(), points.cend(), [&verts] (const sf::Vector2f& p) {
        auto v = std::make_shared<Vertex>();
        v->p = p;
        verts.push_back(v);
    });
}


EdgePtr findOuterEdge(const VertexPtr& v) {
	auto e = v->leavingEdge.lock()->twin.lock();
	auto start = e;
	
	do {
		if (e->face.lock().get() == nullptr) {
			return e->twin.lock();
		}
		e = e->next.lock()->twin.lock();
	} while (e != start);
	return EdgePtr{ nullptr };
}


void generateVoronoiVerticesFromDelaunayFaces(const std::vector<FacePtr>& faces, std::vector<VertexPtr>& vertices, std::unordered_map<Face*, VertexPtr>& map) {
	for (auto& f : faces) {
		auto v = std::make_shared<Vertex>();
		v->p = f->c.c;
		vertices.push_back(v);
		map[f.get()] = v;
	}
}

sf::Vector2f createNewDistantPoint(const EdgePtr& e) {
	auto edgeVector = e->b - e->a;
	normalize(edgeVector);
	auto perpVector = perpendicular(edgeVector);
	perpVector *= -4200.0f;
	return e->face.lock()->c.c + perpVector;
}

void createEdgesForVoronoiCell(FacePtr& f, EdgePtr& firstEdge, EdgePtr& lastEdge, EdgePtr* _e, VertexPtr& p1, std::unordered_map<Face*, VertexPtr>& vmap, std::vector<EdgePtr>& vedges, std::unordered_map<Edge*, EdgePtr>& emap, std::function<bool (EdgePtr)> pred) {
	EdgePtr prevEdge{ nullptr };
	EdgePtr startEdge{ nullptr };
	auto e = *_e;
	auto v1 = p1;
	do {
		auto v2 = vmap[e->face.lock().get()];
		auto newE = std::make_shared<Edge>();
		newE->face = f;
		f->edges.push_back(newE);
		newE->v1 = v1;
		newE->v2 = v2;
		newE->a = v1->p;
		newE->b = v2->p;
		auto ret = emap.try_emplace(e.get(), newE);
		if (!ret.second) { // actually should never happen
			std::cout << "It is already there, Watson" << std::endl;
		}
		vedges.push_back(newE);
		if (prevEdge) {
			prevEdge->next = newE;
			newE->prev = prevEdge;
		}
		else {
			startEdge = newE;
		}
		prevEdge = newE;
		v1 = v2;
		e = e->prev.lock()->twin.lock();
	} while (!pred(e));
	firstEdge = startEdge;
	lastEdge = prevEdge;
	*_e = e;
}

FacePtr makeInfiniteVoronoiCell(const VertexPtr& v, EdgePtr& oe, std::vector<EdgePtr>& vedges, std::unordered_map<Face*, VertexPtr>& vmap, std::unordered_map<Edge*, EdgePtr>& emap,
	std::unordered_map<Edge*, VertexPtr>& outerVrts, std::vector<VertexPtr>& vVerts) {
	EdgePtr e = oe;
	auto f = e->face.lock();

	VertexPtr p1;
	auto it = outerVrts.find(e.get());
	if (it == outerVrts.end()) {
		p1 = std::make_shared<Vertex>();
		p1->p = createNewDistantPoint(e);
		vVerts.push_back(p1);
		outerVrts[e->twin.lock().get()] = p1;
	}
	else {
		p1 = it->second;
	}

	EdgePtr prevEdge{ nullptr };
	EdgePtr firstEdge{ nullptr };
	EdgePtr lastEdge{ nullptr };
	FacePtr cell = std::make_shared<Face>();

	createEdgesForVoronoiCell(cell, firstEdge, lastEdge, &e, p1, vmap, vedges, emap, [] (EdgePtr e){ return e->face.lock().get() == nullptr; });

	it = outerVrts.find(e.get());
	if (it == outerVrts.end()) {
		p1 = std::make_shared<Vertex>();
		p1->p = createNewDistantPoint(e->twin.lock());
		vVerts.push_back(p1);
		outerVrts[e->twin.lock().get()] = p1;
	}
	else {
		p1 = it->second;
	}

	auto p2 = p1;
	p1 = lastEdge->v2.lock();
	EdgePtr infEdge2 = std::make_shared<Edge>();
	emap[e.get()] = infEdge2;
	vedges.push_back(infEdge2);
	infEdge2->v1 = p1;
	infEdge2->v2 = p2;
	infEdge2->face = f;
	f->edges.push_back(infEdge2);
	infEdge2->prev = lastEdge;
	lastEdge->next = infEdge2;

	firstEdge->prev = infEdge2;
	infEdge2->next = firstEdge;
	return cell;
}

FacePtr makeRegularVoronoiCell(const VertexPtr& v, std::vector<EdgePtr>& vedges, std::unordered_map<Face*, VertexPtr>& vmap, std::unordered_map<Edge*, EdgePtr>& emap) {
	auto e = v->leavingEdge.lock();
	auto start = e;
	auto& v1 = vmap[e->twin.lock()->face.lock().get()];
	FacePtr cell = std::make_shared<Face>();


	EdgePtr firstEdge{ nullptr };
	EdgePtr lastEdge{ nullptr };

	createEdgesForVoronoiCell(cell, firstEdge, lastEdge, &e, v1, vmap, vedges, emap, [&start] (EdgePtr e){ return e.get() == start.get(); });

	firstEdge->prev = lastEdge;
	lastEdge->next = firstEdge;
	return cell;
}

void generateVoronoiFromDelaunay(const std::vector<VertexPtr>& dvrts, const std::vector<EdgePtr>& dEdges, const std::vector<FacePtr>& dfaces, std::vector<VertexPtr>& vvrts, std::vector<EdgePtr>& vedges, std::vector<FacePtr>& vfaces) {
	std::unordered_map<Face*, VertexPtr> map;
	generateVoronoiVerticesFromDelaunayFaces(dfaces, vvrts, map);

	std::unordered_map<Edge*, EdgePtr> edgeMap;
	std::unordered_map<Edge*, VertexPtr> outerVertsMap;

	for (auto& v : dvrts) {
		EdgePtr e = findOuterEdge(v);

		if (e.get() != nullptr) { // if the vertex v is outer vertex(from the convex hull), then e is the edge of the convex hull leaving from vertex v, otherwise if e is nullptr, then we are dealing with inner vertex
			vfaces.push_back(makeInfiniteVoronoiCell(v, e, vedges, map, edgeMap, outerVertsMap, vvrts));
		}
		else {
			vfaces.push_back(makeRegularVoronoiCell(v, vedges, map, edgeMap));
		}
	}
}

