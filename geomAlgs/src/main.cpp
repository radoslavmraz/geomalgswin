#include <iostream>
#include "SFML/Graphics.hpp"
#include <vector>
#include <cmath>
#include <algorithm>
#include <random>
#include <chrono>
#include <stack>
#include <utility>
#include <queue>
#include "graham_scan.hpp"
#include "gift_wrapping.hpp"
#include "common_math.hpp"
#include "KdNode.hpp"
#include "delaunay.hpp"
#include "MenuItem.hpp"
#include "triangulation.hpp"
#include <thread>


bool ignoreClickHandler = false;

/*
 * KD tree stuff
 */

KdTree tree;
std::shared_ptr<KdNode> roommate = nullptr;
bool nnSearch = false;
bool rangeSearch = false;
bool showCircles = false;
sf::CircleShape nnQuery;
sf::FloatRect range;
std::vector<sf::Vector2f> rangeNodes;
sf::Vector2f nearestN;
std::vector<sf::CircleShape> considerationCircles;
std::pair<sf::Vector2f, sf::Vector2f> boundary;
bool showKdTree = false;
void drawKdTreeVisLines(KdTree t, sf::RenderWindow &win);
void rebuildKdTree(std::vector<sf::Vector2f>&);
void drawNNSearchResult(sf::RenderWindow&);
void drawRangeSearchResults(sf::RenderWindow&);

/*
 * Menu stuff
 */
std::vector<MenuItem> menu;
sf::RenderWindow menuWindow;
void drawMenu();
void initializeMenu();
void handleMenuEvents(sf::Event&);

/*
 * Convex hull stuff
 */
bool showConvexHull = false;
std::vector<sf::Vector2f> convex_hull;

/*
 * Triangulation of monotone polygons stuff
 */
bool showTriangulation = false;
std::vector<sf::Vector2f> convexHullSorted;
std::vector<sf::Vector2f> right_path;
std::vector<sf::Vector2f> left_path;
std::vector<Line> triang_edges;
std::vector<sf::Color> colors;
bool triangulationMode = 1; //1 for convex hull, 0 for user defined polygon
void triangulateConvexHull();

// For arbitrary polygon triangulation
bool customPolygonMode = false;
std::vector<sf::Vector2f> polygonPoints;
std::vector<sf::Vector2f> polygonPointsSorted;
std::vector<Line> polygonTriangulationLines;

std::vector<sf::Vector2f> points;
sf::Font font;
sf::Vector2f drag_start;
sf::Vector2f drag_end;


sf::Vector2f* randPoint = nullptr;
sf::Vector2f* closestPoint = nullptr;
std::vector<EdgePtr> ael;
std::vector<EdgePtr> dl;
std::vector<Line> dlLines;
std::vector<FacePtr> faces;
std::vector<VertexPtr> dlVerts;
std::vector<sf::Color> circleColors;
bool showDelaunay = false;
std::atomic_bool b;

bool showVoronoi = false;

std::vector<VertexPtr> voronoiVertices;
std::vector<EdgePtr> voronoiEdges;
std::vector<FacePtr> voronoiCells;

void calculateVoronoi() {
	voronoiCells.clear();
	voronoiVertices.clear();
	voronoiEdges.clear();
	generateVoronoiFromDelaunay(dlVerts, dl, faces, voronoiVertices, voronoiEdges, voronoiCells);
}

void triangulateCustomPolygon() {
	triangulatePolygon(polygonPoints, polygonTriangulationLines);
}

void calculateDelaunayTriangulation() {
    ael.clear();
    dl.clear();
    dlLines.clear();
    dlVerts.clear();
    faces.clear();
    delaunay_triangulation2(points, ael, dl, faces, dlVerts);
    std::for_each(dl.begin(), dl.end(), [=] (const EdgePtr& e) {
       Line l = std::make_pair((e->a), (e->b));
       dlLines.push_back(l);
    });
    circleColors.clear();
    std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> color(0, 255);
    for (int i = 0; i < faces.size(); ++i) {
        circleColors.emplace_back(color(gen), color(gen), color(gen), 255);
    }
}


size_t circle_radius = 5;
size_t window_width = 1000;
size_t window_height = 1000;
unsigned int mode = 0; //0 is showing points, 1 is for showing lines
//mouse stuff
bool point_move_mode = false;
bool mouse_pressed = false;
bool dragging = false;
bool showMenu = false;
sf::Vector2f* hovered_point = nullptr;
int hovered_point_idx = -1;


void generateRandomPoints();

void calculate_convex_hull(){
    graham_scan(points, convex_hull);
}

sf::CircleShape delCircle;
sf::CircleShape delPoint;



void handle_key_event(sf::Event& e, sf::RenderWindow& win){
    switch (e.key.code){
        case sf::Keyboard::Escape:
        win.close();
        break;
    }
}

void handle_mouse_moved_event(sf::Event& e, sf::Window& win){
	if (mouse_pressed) {
		ignoreClickHandler = true;
		dragging = true;
	}
    if (dragging) {
        drag_end.x = e.mouseMove.x;
        drag_end.y = e.mouseMove.y;
        if (rangeSearch) {
            range.left = std::min(drag_start.x, drag_end.x);
            range.width = std::max(drag_start.x, drag_end.x);
            range.top = std::min(drag_start.y, drag_end.y);
            range.height = std::max(drag_start.y, drag_end.y);
        }
        if (hovered_point_idx >= 0) {
            auto& p = (customPolygonMode) ? polygonPoints[hovered_point_idx] : points[hovered_point_idx];
            p.x = e.mouseMove.x - circle_radius;
            p.y = e.mouseMove.y - circle_radius;
        }
    }
}

bool is_hovered(const sf::Vector2f& p, const sf::Vector2f& mouse){
    sf::Vector2f v(p.x - mouse.x, p.y - mouse.y);
    return sqrt_magnitude(v) - circle_radius - 2.f < 0.f;
}

void remove_point_at(const sf::Vector2f& p){
    if (hovered_point_idx >= 0) {
		if (customPolygonMode) {
			polygonPoints.erase(polygonPoints.begin() + hovered_point_idx);
		}
		else {
			points.erase(points.begin() + hovered_point_idx);
		}
    }
}

void handle_mouse_pressed_event(sf::Event& e, sf::Window& win){
    mouse_pressed = true;
    drag_start.x = e.mouseButton.x;
    drag_start.y = e.mouseButton.y;
    if (rangeSearch) {
        return;
    }
    hovered_point = nullptr;
    hovered_point_idx = -1;
    sf::Vector2f mouse(e.mouseButton.x - circle_radius, e.mouseButton.y - circle_radius);
	std::vector<sf::Vector2f>& _points = (customPolygonMode) ? polygonPoints : points;
    for (int i = 0; i < _points.size(); ++i){
        sf::Vector2f p = _points[i];
        if (is_hovered(p, mouse)){
            hovered_point_idx = i;
            hovered_point = &p;
            break;
        }
    }
}

void handle_mouse_released_event(sf::Event& e, sf::Window& win) {
    if (dragging) {
        dragging = false;
        if (rangeSearch) {
            drag_end.x = e.mouseButton.x;
            drag_end.y = e.mouseButton.y;
            range.left = std::min(drag_start.x, drag_end.x);
            range.width = std::max(drag_start.x, drag_end.x);
            range.top = std::min(drag_start.y, drag_end.y);
            range.height = std::max(drag_start.y, drag_end.y);
            rangeNodes.clear();
            sf::FloatRect win_reg;
            win_reg.left = 0;
            win_reg.top = 0;
            win_reg.width = window_width;
            win_reg.height = window_height;
            if (tree) {
                rangeNodes = tree.rangeSearch(range, win_reg);
            }
        } else {
            // we are dragging but not performing rangeSearch so that means we are moving some point -> therefore recalculate convex hull, delaunay and voronoi
            calculate_convex_hull();
			calculateDelaunayTriangulation();
			calculateVoronoi();
			if (!customPolygonMode && showTriangulation) {
				triangulateConvexHull();
			}
			if (customPolygonMode) {
				triangulateCustomPolygon();
			}
        }
    } 
    mouse_pressed = false;
}

void handle_mouse_clicked_event(sf::Event& e, sf::Window& win){
	if (ignoreClickHandler) {
		ignoreClickHandler = false;
		return;
	}
	if (nnSearch) {
		nnQuery.setPosition(e.mouseButton.x, e.mouseButton.y);
		roommate = tree.findCellMate(sf::Vector2f(e.mouseButton.x, e.mouseButton.y)); // finds the point that lives inside the region where the NN query is in
		boundary = tree.getCellBoundariesFor(roommate, 0, window_width, 0, window_height); // computes boundary of the region
		nearestN = tree.nnSearch(sf::Vector2f(e.mouseButton.x, e.mouseButton.y), considerationCircles); // considerationCircles -> stores all the circle areas that where considered before the actual nearest neighbor was found
		for (auto &circle : considerationCircles) {
			circle.setPosition(circle.getPosition() + sf::Vector2f(circle_radius, circle_radius));
		}
		boundary.first += sf::Vector2f(circle_radius, circle_radius);
		boundary.second += sf::Vector2f(circle_radius, circle_radius);
	} else {
		switch (e.mouseButton.button){
			case sf::Mouse::Left:
				if (!customPolygonMode) {
					points.push_back(sf::Vector2f(e.mouseButton.x - circle_radius, e.mouseButton.y - circle_radius));
					calculate_convex_hull();
					rebuildKdTree(points);
					calculateDelaunayTriangulation();
					calculateVoronoi();
					if (showTriangulation) {
						triangulateConvexHull();
					}
				}
				else {
					polygonPoints.push_back(sf::Vector2f(e.mouseButton.x - circle_radius, e.mouseButton.y - circle_radius));
				}
				break;
			case sf::Mouse::Right:
				remove_point_at(sf::Vector2f(e.mouseButton.x - circle_radius, e.mouseButton.y - circle_radius));
				if (!customPolygonMode) {
					calculate_convex_hull();
					rebuildKdTree(points);
					calculateDelaunayTriangulation();
					calculateVoronoi();
					if (showTriangulation) {
						triangulateConvexHull();
					}
				}
				break;
		}
	}
}

void rebuildKdTree(std::vector<sf::Vector2f>& points){
    tree.build(points);
    tree.computeVisLines(window_width, window_height);
}

void generateRandomPoints(){
    points.clear();
	faces.clear();
	dlLines.clear();
	dlVerts.clear();
	ael.clear();
	dl.clear();
	circleColors.clear();
    std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<short> distro(5, 150);
    int offset = 50;
    std::uniform_real_distribution<float> width_distro(0 + offset, window_width - offset);
    std::uniform_real_distribution<float> height_distro(0 + offset, window_height - offset);
    short count = distro(generator);
    for (int i = 0; i < count; ++i){
        points.push_back(sf::Vector2f(width_distro(generator), height_distro(generator)));
    }
    std::cout << "Generated " << count << " points" << std::endl;
    tree.build(points);
    tree.computeVisLines(window_width, window_height);
    calculate_convex_hull();
	calculateDelaunayTriangulation();
	calculateVoronoi();
	if (showTriangulation) {
		triangulateConvexHull();
	}
}

std::vector<sf::String> legend_labels {
    "Tab to show/hide menu",
    "left/right click to add/remove a point",
    "M to toggle moving points (mouse drag to move)",
    "R to generate random poitns",
    "C to clear the scene",
    "H to show convex hull",
    "ESC to give up"
    };


void draw_legend(sf::Font& f, sf::RenderWindow& win){
    sf::Vector2f text_pos(10, 10);
    for (auto label : legend_labels){
        sf::Text t(label, f);
        t.setFillColor(sf::Color::Black);
        t.setPosition(text_pos);
        win.draw(t);
        text_pos.y += 40;
        if (!showMenu){
            break;
        }
    }
}

void drawEdges(std::vector<EdgePtr>& edges, sf::Vector2f offset, sf::RenderWindow& win) {
	if (edges.size() == 0) {
		return;
	}
    sf::VertexArray _line(sf::Lines, 2);
	_line[0].color = sf::Color::Blue;
	_line[1].color = sf::Color::Blue;
	for (auto& e : edges) {
		_line[0].position = e->v1.lock()->p + offset;
		_line[1].position = e->v2.lock()->p + offset;
		win.draw(_line);
	}
}

void drawCustomPoly(const std::vector<sf::Vector2f>& _points, sf::RenderWindow& win) {
	if (_points.size() < 2) {
		return;
	}
	sf::VertexArray arr(sf::Lines, 2);
	sf::Vector2f offset(circle_radius, circle_radius);

	arr[0].color = arr[1].color = sf::Color::Black;

	arr[0].position = _points[0] + offset;
	
	for (auto it = _points.cbegin()++; it != _points.cend(); ++it) {
		arr[1].position = *it + offset;
		win.draw(arr);
		arr[0].position = arr[1].position;
	}
	arr[0].color = sf::Color::Red;
	arr[1].color = sf::Color::Red;
	arr[1].position = _points[0] + offset;
	win.draw(arr);
}

void draw_paths(sf::RenderWindow& win){
    sf::CircleShape b;
    b.setRadius(10);
    b.setFillColor(sf::Color::Yellow);
    for (auto v : left_path){
        b.setPosition(v);
        win.draw(b);
    }
    b.setFillColor(sf::Color::Red);
    for (auto v : right_path){
        b.setPosition(v);
        win.draw(b);
    }
}
void draw_points(std::vector<sf::Vector2f>& points, sf::RenderWindow& win) {
    sf::CircleShape circle;
    circle.setPointCount(16);
    circle.setFillColor(sf::Color::Black);
    circle.setRadius(circle_radius);
    for (auto pos : points){
        circle.setPosition(pos);
        win.draw(circle);
    }
}

void draw_point_labels(sf::RenderWindow& win){
    sf::Text t;
    t.setFont(font);
    for (int i = 0; i < points.size(); ++i){
        auto p = points[i];
        t.setString(std::to_string((int)p.x) + " " + std::to_string((int)p.y));
        t.setPosition(p.x + circle_radius, p.y + 15);
        t.setFillColor(sf::Color::Green);
        t.setOutlineColor(sf::Color::Red);
        t.setCharacterSize(24);
        win.draw(t);
    }
}

void draw_convex_hull(sf::RenderWindow& win){
    sf::CircleShape circle;
    circle.setFillColor(sf::Color::Green);
    circle.setOutlineColor(sf::Color::Black);
    circle.setRadius(circle_radius);
    sf::ConvexShape ch;
    ch.setPointCount(convex_hull.size());
    ch.setFillColor(sf::Color::Transparent);
    ch.setOutlineThickness(3);
    ch.setOutlineColor(sf::Color::Black);
    for (int i = 0; i < convex_hull.size(); ++i){
        ch.setPoint(i, sf::Vector2f(convex_hull[i].x + circle_radius, convex_hull[i].y + circle_radius));
    }
    win.draw(ch);
    for (auto ptr : convex_hull){
        circle.setPosition(ptr);
        win.draw(circle);
    }
}

void draw_lines(const std::vector<Line>& lines, sf::RenderWindow& win){
    sf::VertexArray _line(sf::Lines, 2);
    for (const auto& line : lines){
        _line[0].position = line.first + sf::Vector2f((float)circle_radius, (float)circle_radius);
        _line[1].position = line.second + sf::Vector2f((float)circle_radius, (float)circle_radius);
        _line[0].color = _line[1].color = sf::Color::Black;
        win.draw(_line);
    }
}

void redrawScreen(sf::RenderWindow& win) {
	if (customPolygonMode) {
		draw_points(polygonPoints, win);
		drawCustomPoly(polygonPoints, win);
		draw_lines(polygonTriangulationLines, win);
	}
	else {
		draw_points(points, win);
        if (showConvexHull){
            draw_convex_hull(win);
        }
        if (showTriangulation){
            draw_lines(triang_edges, win);
			drawCustomPoly(convex_hull, win);
        }
        if (showKdTree && tree){
            drawKdTreeVisLines(tree, win);
        }
        if (rangeSearch) {
            drawRangeSearchResults(win);
        }
        if (nnSearch && roommate) {
            drawNNSearchResult(win);
        }
        if (showDelaunay) {
            draw_lines(dlLines, win);
        }
        if (showVoronoi) {
			drawEdges(voronoiEdges, sf::Vector2f(circle_radius, circle_radius), win);
        }
	}
}


int main(int, char**) {
    sf::ContextSettings ctxSett(0, 0, 1);
    sf::RenderWindow win;
    win.create(sf::VideoMode(window_width, window_height), "Project Geom Algs", sf::Style::Resize | sf::Style::Close, ctxSett);
	font.loadFromFile("RobotoMono-Regular.ttf");
	menuWindow.create(sf::VideoMode(500, 1000), "MENU", sf::Style::Close);
    initializeMenu();
	nnQuery.setFillColor(sf::Color::Yellow);
	nnQuery.setRadius(circle_radius);
    sf::View v;
    v.reset(sf::FloatRect(0, 0, window_width, window_height));
    while (win.isOpen()){
        win.clear(sf::Color::White);
        sf::Event e;
        if (win.pollEvent(e)){
            switch (e.type){
                case sf::Event::Resized:
                v.reset(sf::FloatRect(0, 0, e.size.width, e.size.height));
                window_width = v.getSize().x;
                window_height = v.getSize().y;
                win.setView(v);
                break;
                case sf::Event::KeyReleased:
                handle_key_event(e, win);
                break;
                case sf::Event::MouseButtonReleased:
				handle_mouse_released_event(e, win);
                handle_mouse_clicked_event(e, win);
                hovered_point = nullptr;
                hovered_point_idx = -1;
                break;
                case sf::Event::MouseButtonPressed:
                handle_mouse_pressed_event(e, win);
                dragging = point_move_mode || rangeSearch;
                break;
                case sf::Event::MouseMoved:
                handle_mouse_moved_event(e, win);
                break;
                case sf::Event::Closed:
                win.close();
                break;
            }
        }
        if (menuWindow.pollEvent(e)) {
            handleMenuEvents(e);
        }
        menuWindow.clear(sf::Color::White);
		redrawScreen(win);
        win.display();
        drawMenu();
        menuWindow.display();
    }
}


void drawKdTreeVisLines(KdTree t, sf::RenderWindow &win) {
    sf::VertexArray arr(sf::Lines, 2);
    arr[0].color = sf::Color::Red;
    arr[1].color = sf::Color::Red;
    auto hlines = t.getHorizontalLines();
    auto vlines = t.getVerticalLines();

    for (const auto& l : hlines) {
        arr[0].position = sf::Vector2f(l.y + circle_radius, l.x + circle_radius);
        arr[1].position = sf::Vector2f(l.z + circle_radius, l.x + circle_radius);
        win.draw(arr);
    }
    arr[0].color = sf::Color::Blue;
    arr[1].color = sf::Color::Blue;

    for (const auto& l : vlines) {
        arr[0].position = sf::Vector2f(l.x + circle_radius, l.y + circle_radius);
        arr[1].position = sf::Vector2f(l.x + circle_radius, l.z + circle_radius);
        win.draw(arr);
    }
}

void clearScene() {
	if (customPolygonMode) {
		polygonPoints.clear();
		polygonTriangulationLines.clear();
		return;
	}
	voronoiCells.clear();
	voronoiVertices.clear();
	voronoiEdges.clear();
    points.clear();
    convex_hull.clear();
    tree = KdTree();
    considerationCircles.clear();
    nearestN = sf::Vector2f(-42, -42);
    rangeNodes.clear();
    range = sf::FloatRect(0, 0, 0, 0);
    triang_edges.clear();
    boundary.first = nearestN;
    boundary.second = nearestN;
    roommate.reset();
    dlVerts.clear();
    ael.clear();
    dl.clear();
    dlLines.clear();
    faces.clear();
}

void initializeMenu() {
    menu.emplace_back("Generate random points", font, generateRandomPoints);
    menu.emplace_back("Show convex hull", font, &showConvexHull);
	auto& chull = menu[1];

    MenuItem showTr1("Show triangulation", font, &showTriangulation);
    MenuItem doTr1("Triangulate", font, triangulateConvexHull);
	chull.pushSubItem(showTr1);
	chull.pushSubItem(doTr1);
    MenuItem showKd("Show K-D Tree", font, &showKdTree);
    MenuItem kdNnSearch("Nearest neighbor search", font, &nnSearch);
	MenuItem nnCircle("Show NN search circles", font, &showCircles);
    showKd.pushSubItem(kdNnSearch);
	showKd.pushSubItem(nnCircle);
    MenuItem kdRangeSearch("Range Search mode", font, &rangeSearch);
    showKd.pushSubItem(kdRangeSearch);
    menu.push_back(showKd);
    menu.emplace_back("Clear points", font, clearScene);
    MenuItem showDel("Show Delaunay tr.", font, &showDelaunay);
    MenuItem showVDiagram("Show Voronoi diagram", font, &showVoronoi);

	MenuItem customPolygon("Custom polygon mode", font, &customPolygonMode);
	MenuItem cpTriangulate("Triangulate", font, &triangulateCustomPolygon);
	customPolygon.pushSubItem(cpTriangulate);
	menu.push_back(customPolygon);

    menu.push_back(showDel);
    menu.push_back(showVDiagram);
    float maxW = 0;
    float maxH = 0;

    for (const auto& mi : menu) {
        auto fr = mi.getBoundRect();
        maxW = std::max(fr.width, maxW);
        maxH = std::max(fr.height, maxH);
		for (const auto& smi : mi.getSubItems()) {
			auto _fr = smi.getBoundRect();
			maxW = std::max(_fr.width, maxW);
			maxH = std::max(_fr.height, maxH);
		}
    }
    sf::FloatRect bo(0, 0, 1.1 * maxW, maxH);

    sf::Vector2f startPos(0, 0);
    for (auto& mi : menu) {
        mi.setMenuItemRect(bo);
        mi.setPosition(startPos + sf::Vector2f(5, 5));
        startPos.y += maxH + 10;
        for (auto& smi : mi.getSubItems()) {
            smi.setMenuItemRect(bo);
            smi.setPosition(startPos + sf::Vector2f(40, 5));
            startPos.y += maxH + 10;
        }
    }
}

void drawMenu() {
    for (const auto& mi : menu) {
        mi.draw(menuWindow);
        for (const auto& smi : mi.getSubItems()) {
            smi.draw(menuWindow);
        }
    }
}

void handleMenuEvents(sf::Event& e) {
    switch (e.type) {
        case sf::Event::MouseButtonReleased:
            sf::Vector2f m(e.mouseButton.x, e.mouseButton.y);
            for (auto& mi : menu) {
                if (mi.checkAndHandleClick(m)) {
                    return;
                }
                for (auto& smi : mi.getSubItems()) {
                    if (smi.checkAndHandleClick(m)) {
                        return;
                    }
                }
            }
            break;
    }
}

void triangulateConvexHull() {
	triangulatePolygon(convex_hull, triang_edges);
}


void drawNNSearchResult(sf::RenderWindow& win) {
	win.draw(nnQuery);
    sf::CircleShape c;
    c.setPosition(roommate->data.x, roommate->data.y); // draws the inhabitant of the region where the NN query happened
    c.setRadius(5.0f);
    c.setFillColor(sf::Color::Cyan);
    //win.draw(c);
    c.setPosition(nearestN.x, nearestN.y);
    c.setFillColor(sf::Color::Green);
    win.draw(c);
    sf::RectangleShape b;
    b.setPosition(boundary.first);
    b.setSize(boundary.second - boundary.first);
    b.setOutlineThickness(2.0f);
    b.setOutlineColor(sf::Color::Green);
    b.setFillColor(sf::Color::Transparent);
    win.draw(b);
	if (showCircles) {
		std::for_each(considerationCircles.begin(), considerationCircles.end(), [&win](auto& c) { c.setOutlineColor(sf::Color::Black); win.draw(c); });
	}
}

void drawRangeSearchResults(sf::RenderWindow& win) {
    sf::RectangleShape rect;
    rect.setPosition(range.left, range.top);
    rect.setSize(sf::Vector2f(range.width - range.left, range.height - range.top));
    rect.setOutlineColor(sf::Color::Black);
    rect.setOutlineThickness(1.5f);
    rect.setFillColor(sf::Color(255, 255, 0, 50));
    win.draw(rect);
    if (rangeNodes.size() > 0) {
        sf::CircleShape c;
        c.setFillColor(sf::Color::Cyan);
        c.setRadius(circle_radius);
        for (const auto& n : rangeNodes) {
            c.setPosition(n);
            win.draw(c);
        }
    }
}
