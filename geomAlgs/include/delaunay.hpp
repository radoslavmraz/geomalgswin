//
// Created by radoslav on 29.11.2018.
//

#ifndef GEOMALGS_DELAUNAY_HPP
#define GEOMALGS_DELAUNAY_HPP
#include "SFML/Graphics.hpp"
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <memory>
#include <iostream>


struct Face;
struct Vertex;
struct Edge;

using EdgePtr = std::shared_ptr<Edge>;
using EdgeWPtr = std::weak_ptr<Edge>;
using VertexPtr = std::shared_ptr<Vertex>;
using VertexWPtr = std::weak_ptr<Vertex>;
using FacePtr = std::shared_ptr<Face>;
using FaceWPtr = std::weak_ptr<Face>;

struct Circle {
    sf::Vector2f c;
    float r;

    bool isInside(const sf::Vector2f&)const;
};
struct Face {
    Circle c;
	std::vector<EdgeWPtr> edges;
};

struct Vertex {
    sf::Vector2f p;
    EdgeWPtr leavingEdge;
};

struct Edge {
    VertexWPtr v1;
    VertexWPtr v2;
    sf::Vector2f a;
    sf::Vector2f b;

    FaceWPtr face;
    EdgeWPtr twin;
    EdgeWPtr next;
    EdgeWPtr prev;
};



Circle circumCircle(sf::Vector2f, sf::Vector2f, sf::Vector2f);

size_t chooseRandomPointIdx(std::vector<sf::Vector2f>&);
VertexPtr getClosestPoint(std::vector<VertexPtr>& points, sf::Vector2f p);
void delaunay_triangulation2(std::vector<sf::Vector2f>& points, std::vector<EdgePtr>& ael, std::vector<EdgePtr>& dl, std::vector<FacePtr>&, std::vector<VertexPtr>&);
void opposify(EdgePtr e, std::vector<EdgePtr>&);
void facify(EdgePtr, VertexPtr, std::vector<EdgePtr>&, std::vector<FacePtr>&, std::vector<EdgePtr>&);
void generateVertices(const std::vector<sf::Vector2f>&, std::vector<VertexPtr>&);

void generateVoronoiFromDelaunay(const std::vector<VertexPtr>&, std::vector<VertexPtr>&, std::vector<EdgePtr>&, std::vector<FacePtr>&);
void generateVoronoiFromDelaunay(const std::vector<VertexPtr>& dvrts, const std::vector<EdgePtr>& dEdges, const std::vector<FacePtr>& dfaces, std::vector<VertexPtr>& vvrts, std::vector<EdgePtr>& vedges, std::vector<FacePtr>& vfaces);
#endif //GEOMALGS_DELAUNAY_HPP
