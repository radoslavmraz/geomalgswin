#include <cmath>
#ifndef __COMMON_MATH__
#define __COMMON_MATH__
#include "SFML/Graphics.hpp"
#ifndef M_PI
namespace
{
	const double M_PI = std::acos(-1.0);
}
#endif

void normalize(sf::Vector2f&);
float angle_between(const sf::Vector2f&, const sf::Vector2f&);
float dot_product(const sf::Vector2f& v1, const sf::Vector2f& v2);

float sqrt_magnitude(const sf::Vector2f& v);

float cos_of_angle(const sf::Vector2f& v1, const sf::Vector2f& v2);
bool is_right_turn(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3);


float distance(sf::Vector2f, sf::Vector2f);
bool equal(const sf::Vector2f&, const sf::Vector2f&);
sf::Vector2f perpendicular(const sf::Vector2f&);
float pseudoCrossProduct(sf::Vector2f, sf::Vector2f, sf::Vector2f);
#endif