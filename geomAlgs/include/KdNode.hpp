#ifndef __GEOM_KDNODE__
#define __GEOM_KDNODE__
#include "SFML/Graphics.hpp"
#include <memory>

using pointsIterator = std::vector<sf::Vector2f>::iterator;
struct KdNode {
    int k {2};
    int depth {0};
    sf::Vector2f data;
    std::shared_ptr<KdNode> parent {nullptr};
    std::shared_ptr<KdNode> left {nullptr}; //lesser
    std::shared_ptr<KdNode> right {nullptr}; //greater

};

class KdTree {
private:
    std::shared_ptr<KdNode> root;

    std::vector<sf::Vector3f> hLines;
    std::vector<sf::Vector3f> vLines;

    std::shared_ptr<KdNode> buildKdTree(std::vector<sf::Vector2f>&, std::shared_ptr<KdNode>, pointsIterator, pointsIterator, int);
    void collectLeaves(std::shared_ptr<KdNode>, std::vector<sf::Vector2f>&);

public:
    void build(std::vector<sf::Vector2f>&);
    std::shared_ptr<KdNode> findCellMate(const sf::Vector2f&);
    std::pair<sf::Vector2f, sf::Vector2f> getCellBoundariesFor(std::shared_ptr<KdNode>, float, float, float, float);
    sf::Vector2f nnSearch(sf::Vector2f, std::vector<sf::CircleShape>&);
    std::vector<sf::Vector2f> rangeSearch(sf::FloatRect, sf::FloatRect);
    static bool isLeaf(std::shared_ptr<KdNode>);
    void computeVisLines(float, float);

    const std::vector<sf::Vector3f>& getHorizontalLines() { return hLines; }
    const std::vector<sf::Vector3f>& getVerticalLines() { return vLines; }

    explicit operator bool() const noexcept { return (root.get() != nullptr); };


};

/*KdNode* buildKdTree(std::vector<sf::Vector2f>& points, KdNode* parent, pointsIterator start, pointsIterator end, int depth);
KdNode* findCellMate(KdNode *, sf::Vector2f);
std::pair<sf::Vector2f, sf::Vector2f> getCellBoundariesFor(KdNode*, float, float, float, float);
KdNode* nnSearch(KdNode* root, sf::Vector2f p, std::vector<sf::CircleShape>&);
void collectLeaves(KdNode*, std::vector<sf::Vector2f>&);
std::vector<sf::Vector2f> rangeSearch(KdNode*, sf::FloatRect, sf::FloatRect);
void computeVisLines(KdNode*, std::vector<sf::Vector3f>&, std::vector<sf::Vector3f>&, float, float);*/
#endif