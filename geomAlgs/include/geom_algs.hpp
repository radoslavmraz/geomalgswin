#include <vector>
#include <dequee>
#include "SFML/Graphics.hpp"

float angle_between(const sf::Vector2f&, const sf::Vector2f&);
void normalize(sf::Vector2f&);
bool is_right_turn(const sf::Vector2f&, const sf::Vector2f&, const sf::Vector2f&);
bool is_right_turn(std::deque<sf::Vector2f*>&);

void calculate_chull_GW(const std::vector<sf::Vector2f>&, std::vector<sf::Vector2f>);
void calculate_chull_GrahamScan
void generate_random_point(std::vector<sf::Vector2f>&);

