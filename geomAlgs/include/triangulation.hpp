//
// Created by radoslav on 28.12.2018.
//

#ifndef GEOMALGS_TRIANGULATION_H
#define GEOMALGS_TRIANGULATION_H


#include <SFML/System/Vector2.hpp>
#include <vector>

using Line = std::pair<sf::Vector2f, sf::Vector2f>;
void triangulatePolygon(std::vector<sf::Vector2f>&, std::vector<Line>&);
bool is_from_right_chain(const sf::Vector2f&, const std::vector<sf::Vector2f>&);
#endif //GEOMALGS_TRIANGULATION_H
