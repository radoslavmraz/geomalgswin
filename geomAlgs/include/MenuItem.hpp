//
// Created by radoslav on 28.12.2018.
//

#ifndef GEOMALGS_MENUITEM_H
#define GEOMALGS_MENUITEM_H

#include <SFML/Graphics/Text.hpp>
#include <memory>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

//using clickHandler = void (*)(sf::Event&, sf::RenderWindow&);
using clickHandler = void (*)();
class MenuItem {
private:
    sf::Text text;
    sf::Font font;
    sf::RectangleShape background;
    sf::FloatRect rect;
    std::vector<MenuItem> subItems;
    clickHandler handler {nullptr};
    bool* boolSwitch {nullptr};
    void initRect();
public:
    MenuItem(const std::string&, sf::Font&, clickHandler);
    MenuItem(const std::string&, sf::Font&, bool*);
    bool checkAndHandleClick(const sf::Vector2f&);
    void draw(sf::RenderWindow&)const;
    const sf::FloatRect getBoundRect() const { return text.getGlobalBounds(); }
    void pushSubItem(MenuItem);
    void setPosition(const sf::Vector2f&);
    std::vector<MenuItem>& getSubItems() { return subItems; }
    const std::vector<MenuItem>& getSubItems() const { return subItems; }
    void setMenuItemRect(sf::FloatRect);
};


#endif //GEOMALGS_MENUITEM_H
